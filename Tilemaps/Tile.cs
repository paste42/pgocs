﻿namespace PGOCS.Tilemaps
{
    public class Tile
    {
        public enum TileStatus
        {
            Empty,
            Solid,
        }

        public TileStatus Status { get; set; }
        public int Id { get; set; }

        public Tile(int id, TileStatus status = TileStatus.Empty)
        {
            Id = id;
            Status = status;
        }

    }
}
