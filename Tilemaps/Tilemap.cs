﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using TiledSharp;

namespace PGOCS.Tilemaps
{
    public class Tilemap
    {
        Tile[,] _tiles;
        public int Height { get; private set; }
        public int Width { get; private set; }

        public int TileHeight { get; private set; }
        public int TileWidth { get; private set; }
        public int MaxID { get; private set; }

        /// <summary>
        /// Create an empty Tilemap
        /// </summary>
        public Tilemap(Point mapSize, Point tileSize)
        {
            TileWidth = tileSize.X;
            TileHeight = tileSize.Y;

            Width = mapSize.X;
            Height = mapSize.Y;

            InitTiles(null);
        }

        /// <summary>
        /// Create a new Tilemap from a TiledSharp.TmxMap
        /// </summary>
        /// <param name="tmxMap"></param>
        public Tilemap(TmxMap tmxMap)
        {
            //tileSize = new Point(tmxMap.TileWidth, tmxMap.TileHeight);
            TileWidth = tmxMap.TileWidth;
            TileHeight = tmxMap.TileHeight;

            Width = tmxMap.Width;
            Height = tmxMap.Height;

            InitTiles(tmxMap);
        }

        private void InitTiles(TmxMap tmxMap)
        {
            _tiles = new Tile[Width, Height];
            MaxID = 0;

            for (int i = 0; i < Height; ++i)
                for (int j = 0; j < Width; ++j)
                {
                    _tiles[j, i] = tmxMap == null ? new Tile(0) : new Tile(tmxMap.Layers[0].Tiles[j + i * Width].Gid);
                    MaxID = MathHelper.Max(MaxID, _tiles[j, i].Id);
                }
        }

        public Tile GetTile(int col, int row)
        {
	        if ((col < Width) && (row < Height) && (col >= 0) && (row >= 0)) return _tiles[col, row];
	        Debug.WriteLine("Coordinate (" + col + ", " + row + ") out of Tilemap range.");
	        return null;
        }

	    public Tile GetTile(Vector2 position)
	    {
		    if ((!(position.X < 0)) && (!(position.X > TileWidth*Width)) && (!(position.Y < 0)) &&
		        (!(position.Y > TileHeight*Height))) return GetTile((int) position.X/TileWidth, (int) position.Y/TileHeight);
		    Debug.WriteLine("Position " + position + " out of Tilemap range.");
		    return null;
	    }

	    public Point GetTileCoords(Vector2 position)
	    {
		    return new Point((int)position.X/TileWidth, (int)position.Y/TileHeight);
	    }

        public void SetTileState(int col, int row, Tile.TileStatus status)
        {
            if ((col >= Width) || (row >= Height))
            {
                Debug.WriteLine("Coordinate (" + col + ", " + row + ") out of Tilemap range.");
                return;
            }
            _tiles[col,row].Status = status;
        }

	    public void SetTileState(Point tilePosition, Tile.TileStatus status)
	    {
		    SetTileState(tilePosition.X, tilePosition.Y, status);
	    }

        public void SetTileId(int col, int row, int id)
        {
            _tiles[col, row].Id = id;
        }

	    public bool IsInBounds(Point tileCoords)
	    {
		    return ((tileCoords.X >= 0) && (tileCoords.X < Width) && (tileCoords.Y >= 0) && (tileCoords.Y < Height));
	    }
    }
}
