﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using PGOCS.Collisions;
using PGOCS.Config;
using PGOCS.Graphics;
using PGOCS.Input;
using PGOCS.Scenes;

namespace PGOCS
{
    public class PGame<TConfig> : Game, IGame
    {
	    public static float GameScale { get; protected set; }
        protected Renderer Renderer;
        protected Scene CurrentScene;
        public DirConfigLoader DirConfig { get; protected set; }

	    protected override void Initialize()
        {
            Renderer = new Renderer(this, GameScale);
			StartConfigLoaderThread();
            base.Initialize();
        }

	    [Conditional("DEBUG")]
	    private void StartConfigLoaderThread()
	    {
			GameConfigLoader<TConfig>.Instance.Start();
	    }

		[Conditional("DEBUG")]
		private void StopConfigLoaderThread()
		{
			GameConfigLoader<TConfig>.Instance.Stop();
		}

	    protected override void Update(GameTime gameTime)
        {
            InputManager.Update();
			CheckColliderTextureToggle();
            CurrentScene.Update(gameTime);
			CurrentScene.LateUpdate(gameTime);
            base.Update(gameTime);
        }

		[Conditional("DEBUG")]
	    private void CheckColliderTextureToggle()
	    {
			if (KeyPresses.WasKeyPressed(Keys.C)) ColliderTexture.ToggleVisible(this);
	    }

        protected override void Draw(GameTime gameTime)
        {
            Renderer.Draw(this);
            CurrentScene.Draw(gameTime);
            base.Draw(gameTime);
            Renderer.PostDraw(this, CurrentScene.Scale);
        }

        public void SwitchScene(Scene scene)
        {
            CurrentScene = scene;
			scene.ResetScene();
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
			StopConfigLoaderThread();
            base.OnExiting(sender, args);
        }
    }
}
