﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Scenes;

namespace PGOCS
{
    public abstract class GameObject : IPausable
    {
        public Transform Transform { get; set; }
		public Scene Scene { get; protected set; }
        public string Name;
        protected GameObject(string id, IGame game, Scene scene, Vector2 position = new Vector2())
        {
            Name = id;
	        Scene = scene;
            componentList = new Dictionary<string, GOComponent>();
            ChildList = new List<GameObject>();
	        Transform = new Transform {Position = position};
	        IsPaused = false;
        }

        /// <summary>
        /// Whether or not a component of type t is attached to this game object
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool HasComponent<T>()
        {
            return (componentList.ContainsKey(typeof(T).FullName));
        }

		/// <summary>
		/// Returns a GOComponent of type T
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
        public T GetComponent<T>() where T : GOComponent
        {
            if (componentList.ContainsKey(typeof(T).FullName))
				return (T)componentList[typeof(T).FullName];
            Debug.WriteLine("Component \"" + typeof(T).FullName + "\" not found in GameObject " + Name);
            return null;
        }

        /// <summary>
        /// Adds a component to the GameObject's list of attached components if one does not exist already.
        /// </summary>
        /// <param name="goc">The component to be added</param>
        public void AddComponent(GOComponent goc)
        {
	        if (goc == null) return;
	        if (componentList.ContainsKey(goc.GetType().FullName))
	        {
		        Debug.WriteLine("GameObject " + Name + " already contains GOComponent of type " + goc.GetType().FullName);
		        return;
	        }
	        componentList.Add(goc.GetType().FullName, goc);
			Debug.WriteLine("GOComponent " + goc.GetType().FullName + " added to GameObject " + Name);
			goc.GameObject = this;
        }

	    public void RemoveComponent(Type type)
	    {
		    if (!componentList.ContainsKey(type.FullName))
		    {
			    Debug.WriteLine("Attempted to remove GOComponent of type " + type.FullName + " from GameObject " + Name 
					+ " but none was found.");
				return;
		    }
		    componentList.Remove(type.FullName);
			Debug.WriteLine("GOComponent " + type.FullName + " removed from GameObject " + Name);
	    }

        /// <summary>
        /// The list of components attached to the GameObject, with type as key
        /// </summary>
        private Dictionary<string, GOComponent> componentList;

		public List<GameObject> ChildList { get; protected set; }

        public void AddChild(GameObject go)
        {
            ChildList.Add(go);
            Transform.AddChild(go.Transform);
        }

        public void RemoveChild(GameObject go)
        {
            ChildList.Remove(go);
            Transform.RemoveChild(go.Transform);
        }

        public virtual void Update(GameTime gameTime)
        {
			foreach (GOComponent goc in componentList.Values)
                goc.Update(gameTime);

            foreach (GameObject go in ChildList)
                go.Update(gameTime);
        }

	    public virtual void LateUpdate(GameTime gameTime)
	    {
			foreach (KeyValuePair<string, GOComponent> goc in componentList)
				goc.Value.LateUpdate(gameTime);

			foreach (GameObject go in ChildList)
				go.LateUpdate(gameTime);
			Transform.Update(gameTime);
	    }

        public virtual void Draw(SpriteBatch sb, Camera camera)
        {
            // Draw the sprite
            var sprite = GetComponent<Sprite>();
            if (sprite != null) sprite.Draw(sb, Transform, camera);

            // Collider Overlay
            DrawColliderOverlay(sb, camera, sprite);

            // Draw the child objects
            foreach(GameObject go in ChildList)
            {
                go.Draw(sb, camera);
            }
        }

        public void DrawColliderOverlay(SpriteBatch sb, Camera camera, Sprite sprite)
        {
            // Collider Overlay
            if (ColliderTexture.IsVisible && sprite.GameObject.HasComponent<Collider>())
            {
                var c = GetComponent<Collider>();
                if (c != null) c.Draw(sb, Transform.GetAbsoluteTransform(), camera);
            }
        }

	    public bool IsPaused { get; set; }

	    public void Pause()
	    {
		    if (Scene is IPausable) IsPaused = ((IPausable) Scene).IsPaused;
	    }
    }
}
