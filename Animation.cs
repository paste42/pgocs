﻿using System;
using System.Text;
using Microsoft.Xna.Framework;

namespace PGOCS
{
    public class Animation
    {
        public int CurrentFrame 
        { 
            get 
            {
                if (_currentFrame >= _frameSequence.Length)
                    throw(new Exception("currentFrame out of bounds of animation " + this));
                return _frameSequence[_currentFrame]; 
            }
            set
            {
                _currentFrame = value;
                _frameTimer = 0;
            }
        }
        public bool IsPlaying { get; set; }
        public bool IsLooping { get; set; }

        private int _frameTimer;
        private int _currentFrame;
        private int[] _frameSequence;
        private int _millisecondsPerFrame;
        public Animation(int[] frameSequence, int millisecondsPerFrame)
        {
            IsPlaying = false;
            _frameSequence = frameSequence;
            _millisecondsPerFrame = millisecondsPerFrame;
            _frameTimer = 0;
        }

        public override string ToString()
        {
            StringBuilder order = new StringBuilder();
            foreach (int i in _frameSequence)
            {
                order.Append(i);
                order.Append(", ");
            }
            order.Remove(order.Length - 2, 2);
            return order.ToString();
        }

        public void Stop()
        {
            IsPlaying = false;
            _frameTimer = 0;
        }

        public void Update(GameTime gameTime)
        {
            if (IsPlaying)
            {
                _frameTimer += gameTime.ElapsedGameTime.Milliseconds;
                if (_frameTimer >= _millisecondsPerFrame)
                {
                    _frameTimer -= _millisecondsPerFrame;
                    ++_currentFrame;
                    if (_currentFrame >= _frameSequence.Length)
                    {
                        if (IsLooping)
                            _currentFrame -= _frameSequence.Length;
                        else
                        {
                            --_currentFrame;
                            Stop();
                        }
                    }
                }
            }
        }

    }
}
