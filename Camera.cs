﻿using Microsoft.Xna.Framework;
using PGOCS.Components;
using PGOCS.Scenes;
using System;

namespace PGOCS
{
    public class Camera : GameObject
    {
        public Vector2 Size { get; private set; }

        public GameObject FocusedObject { get; set; }
        public Vector2 FocusOffset { get; set; }

        /// <summary>
        /// A camera object, describing the area of the game world to be put to the screen.
        /// </summary>
        /// <param name="id">A string identifying the Camera</param>
        /// <param name="size">The size of the viewing area, in pixels. Default is the size of the back buffer.</param>
        /// <param name="position">The upper left corner of the camera. Defaults to (0, 0).</param>
        public Camera(string id, IGame game, Scene scene, Vector2? size = null, Vector2 position = new Vector2()) 
			: base(id, game, scene, position)
        {
            FocusedObject = null;
            FocusOffset = Vector2.Zero;
            Size = size ?? new Vector2(GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight);
        }

        public void Update(GameTime gameTime, Vector2 sceneSize)
        {
            if (FocusedObject != null)
                CenterOn(FocusedObject.Transform.Position + FocusOffset);

            // Keep within bounds
			if (Size.X > sceneSize.X) Transform.Position = new Vector2(-(Size.X - sceneSize.X)/2, Transform.Position.Y);
            else if (Transform.Position.X < 0) Transform.Position = new Vector2(0f, Transform.Position.Y);
            else if (Transform.Position.X + Size.X > sceneSize.X) Transform.Position = new Vector2(sceneSize.X - Size.X, Transform.Position.Y);
			if (Size.Y > sceneSize.Y) Transform.Position = new Vector2(Transform.Position.X, -(Size.Y - sceneSize.Y) / 2);
			else if (Transform.Position.Y < 0) Transform.Position = new Vector2(Transform.Position.X, 0f);
            else if (Transform.Position.Y + Size.Y > sceneSize.Y) Transform.Position = new Vector2(Transform.Position.X, sceneSize.Y - Size.Y);
            Transform.Position = new Vector2((float)Math.Round(Transform.Position.X), (float)Math.Round(Transform.Position.Y));
            base.Update(gameTime);
        }

        public void CenterOn(Vector2 location)
        {
            SetPosition(new Vector2(location.X - Size.X / 2f, location.Y - Size.Y / 2f));
        }

        /// <summary>
        /// Returns the transform relative to the screen (drawing coordinates)
        /// </summary>
        /// <param name="t">The absolute positioned transform to ocnvert to screen coordinates</param>
        /// <returns></returns>
        public Transform GetScreenTransform(Transform t)
        {
            return Transform.Compose(-Transform, t);//.GetAbsoluteTransform());
        }

        public Vector2 GetScreenPosition(Vector2 objPosition)
        {
            return objPosition - Transform.Position;
        }

        public void SetPosition(Vector2 pos)
        {
            Transform.Position = pos;
        }

        public void SetRotation(float rotation)
        {
            Transform.Rotation = rotation;
        }
    }
}
