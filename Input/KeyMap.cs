﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace PGOCS.Input
{
	public static class KeyMap
	{
		private static readonly Dictionary<CommandDelegate, Keys> Keymap;

		static KeyMap()
		{
			Keymap = new Dictionary<CommandDelegate, Keys>();
		}

		public static void AssignKey(Keys key, CommandDelegate command)
		{
			if (Keymap.ContainsKey(command)) Keymap[command] = key;
			else Keymap.Add(command, key);
		}

		public static Keys? GetKey(CommandDelegate command)
		{
			if (!Keymap.ContainsKey(command)) return null;
			return Keymap[command];
		}
	}
}
