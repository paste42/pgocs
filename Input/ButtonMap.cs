﻿using System.Collections.Generic;

namespace PGOCS.Input
{
	public static class ButtonMap
	{
		private static readonly Dictionary<CommandDelegate, ButtonPresses.ButtonPress> Buttonmap;

		static ButtonMap()
		{
			Buttonmap = new Dictionary<CommandDelegate, ButtonPresses.ButtonPress>();
		}

		public static void AssignButton(ButtonPresses.ButtonPress buttonPress, CommandDelegate command)
		{
			if (Buttonmap.ContainsKey(command)) Buttonmap[command] = buttonPress;
			else Buttonmap.Add(command, buttonPress);
		}

		public static ButtonPresses.ButtonPress? GetButtonPress(CommandDelegate command)
		{
			if (!Buttonmap.ContainsKey(command)) return null;
			return Buttonmap[command];
		}
	}
}
