﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PGOCS.Input
{
	public delegate void CommandDelegate(GameObject gameObject);

	public static class InputManager
	{
		public static bool WasInputPressed(CommandDelegate command, PlayerIndex playerIndex = PlayerIndex.One)
		{
			return GetInputStatus(ButtonPresses.WasButtonPressed, KeyPresses.WasKeyPressed, command, playerIndex);
		}

		public static bool WasInputHeld(CommandDelegate command, PlayerIndex playerIndex = PlayerIndex.One)
		{
			return GetInputStatus(ButtonPresses.WasButtonHeld, KeyPresses.WasKeyHeld, command, playerIndex);
		}

		public static bool IsInputPressed(CommandDelegate command, PlayerIndex playerIndex = PlayerIndex.One)
		{
			return GetInputStatus(ButtonPresses.IsButtonDown, KeyPresses.IsKeyDown, command, playerIndex);
		}

		private static bool GetInputStatus(Func<Buttons, PlayerIndex, bool> buttonFunc, Func<Keys, bool> keyFunc,
			CommandDelegate command, PlayerIndex playerIndex)
		{
			ButtonPresses.ButtonPress? bp = ButtonMap.GetButtonPress(command);
			Keys? kp = KeyMap.GetKey(command);
			return (bp != null && buttonFunc(((ButtonPresses.ButtonPress)bp).Button, playerIndex)) ||
				(kp != null && keyFunc((Keys)kp));
		}

		public static void PressInputUntilChecked(CommandDelegate command)
		{
			Keys? key = KeyMap.GetKey(command);
			if (key != null) KeyPresses.PressKeyUntilChecked((Keys)key);
			else Debug.WriteLine("No key assigned to action " + command.Method.Name);
			ButtonPresses.ButtonPress? buttonPress = ButtonMap.GetButtonPress(command);
			if (buttonPress != null) ButtonPresses.PressButtonUntilChecked((ButtonPresses.ButtonPress)buttonPress);
			else Debug.WriteLine("No button assigned to action " + command.Method.Name);
		}

		public static void Update()
		{
			ButtonPresses.Update();
			KeyPresses.Update();
		}
	}
}
