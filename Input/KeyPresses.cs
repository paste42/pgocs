﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace PGOCS.Input
{
    public static class KeyPresses
    {
        static KeyboardState _currKbState;
        static KeyboardState _prevKbState;
	    private static List<Keys> _pressedKeys; 

        static KeyPresses()
        {
            _currKbState = Keyboard.GetState();
            _prevKbState = _currKbState;
			_pressedKeys = new List<Keys>();
        }

        public static void Update()
        {
            _prevKbState = _currKbState;
            _currKbState = Keyboard.GetState();
        }

        public static bool WasKeyPressed(Keys key)
        {
	        if (!_pressedKeys.Contains(key)) return _prevKbState.IsKeyUp(key) && _currKbState.IsKeyDown(key);
	        _pressedKeys.Remove(key);
	        return true;
        }

        public static bool WasKeyHeld(Keys key)
        {
            return _prevKbState.IsKeyDown(key) && _currKbState.IsKeyDown(key);
        }

	    public static bool IsKeyDown(Keys key)
	    {
		    return _currKbState.IsKeyDown(key);
	    }

	    public static void PressKeyUntilChecked(Keys key)
	    {
			if (!_pressedKeys.Contains(key)) _pressedKeys.Add(key);
	    }
    }
}
