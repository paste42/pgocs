﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PGOCS.Input
{
    public class ButtonPresses
    {
	    public struct ButtonPress
	    {
		    public Buttons Button { get; private set; }
		    public PlayerIndex Player { get; private set; }

		    public ButtonPress(Buttons button, PlayerIndex player) : this()
		    {
			    Button = button;
			    Player = player;
		    }
	    }

        static GamePadState _currGamePadState1;
        static GamePadState _currGamePadState2;
        static GamePadState _currGamePadState3;
        static GamePadState _currGamePadState4;
        static GamePadState _prevGamePadState1;
        static GamePadState _prevGamePadState2;
        static GamePadState _prevGamePadState3;
        static GamePadState _prevGamePadState4;
	    private static readonly List<ButtonPress> PressedButtons; 

        static ButtonPresses()
        {
			PressedButtons = new List<ButtonPress>();
            _currGamePadState1 = GamePad.GetState(PlayerIndex.One);
            _currGamePadState2 = GamePad.GetState(PlayerIndex.Two);
            _currGamePadState3 = GamePad.GetState(PlayerIndex.Three);
            _currGamePadState4 = GamePad.GetState(PlayerIndex.Four);
            _prevGamePadState1 = _currGamePadState1;
            _prevGamePadState2 = _currGamePadState2;
            _prevGamePadState3 = _currGamePadState3;
            _prevGamePadState4 = _currGamePadState4;
        }

        public static void Update()
        {
            _prevGamePadState1 = _currGamePadState1;
            _prevGamePadState2 = _currGamePadState2;
            _prevGamePadState3 = _currGamePadState3;
            _prevGamePadState4 = _currGamePadState4;
            _currGamePadState1 = GamePad.GetState(PlayerIndex.One);
            _currGamePadState2 = GamePad.GetState(PlayerIndex.Two);
            _currGamePadState3 = GamePad.GetState(PlayerIndex.Three);
            _currGamePadState4 = GamePad.GetState(PlayerIndex.Four);
        }

        public static bool WasButtonPressed(Buttons button, PlayerIndex playerIndex = PlayerIndex.One)
        {
	        var buttonPress = new ButtonPress(button, playerIndex);
	        if (PressedButtons.Contains(buttonPress))
	        {
		        PressedButtons.Remove(buttonPress);
		        return true;
	        }
            switch (playerIndex)
            {
                case PlayerIndex.One: return ((_prevGamePadState1.IsButtonUp(button)) && _currGamePadState1.IsButtonDown(button));
                case PlayerIndex.Two: return ((_prevGamePadState2.IsButtonUp(button)) && _currGamePadState2.IsButtonDown(button));
                case PlayerIndex.Three: return ((_prevGamePadState3.IsButtonUp(button)) && _currGamePadState3.IsButtonDown(button));
                case PlayerIndex.Four: return ((_prevGamePadState4.IsButtonUp(button)) && _currGamePadState4.IsButtonDown(button));
                default: return ((_prevGamePadState1.IsButtonUp(button)) && _currGamePadState1.IsButtonDown(button));
            }
        }

        public static bool WasButtonHeld(Buttons button, PlayerIndex index = PlayerIndex.One)
        {
            switch (index)
            {
                case PlayerIndex.One: return (_prevGamePadState1.IsButtonDown(button) && _currGamePadState1.IsButtonDown(button));
                case PlayerIndex.Two: return (_prevGamePadState2.IsButtonDown(button) && _currGamePadState2.IsButtonDown(button));
                case PlayerIndex.Three: return (_prevGamePadState3.IsButtonDown(button) && _currGamePadState3.IsButtonDown(button));
                case PlayerIndex.Four: return (_prevGamePadState4.IsButtonDown(button) && _currGamePadState4.IsButtonDown(button));
                default: return (_prevGamePadState1.IsButtonDown(button) && _currGamePadState1.IsButtonDown(button));
            }
        }

	    public static bool IsButtonDown(Buttons button, PlayerIndex playerIndex = PlayerIndex.One)
	    {
			switch (playerIndex)
			{
				case PlayerIndex.One: return _currGamePadState1.IsButtonDown(button);
				case PlayerIndex.Two: return _currGamePadState2.IsButtonDown(button);
				case PlayerIndex.Three: return _currGamePadState3.IsButtonDown(button);
				case PlayerIndex.Four: return _currGamePadState4.IsButtonDown(button);
				default: return _currGamePadState1.IsButtonDown(button);
			}

	    }
		public static void PressButtonUntilChecked(ButtonPress buttonPress)
		{
			if (!PressedButtons.Contains(buttonPress)) PressedButtons.Add(buttonPress);
		}
    }
}
