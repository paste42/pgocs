﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using PGOCS.Input;

namespace PGOCS
{
	public static class DebugConsole
	{
		public delegate void ConsoleCommand();

		private static readonly List<ConsoleCommand> ConsoleCommands;

		static DebugConsole()
		{
			ConsoleCommands = new List<ConsoleCommand>();
		}

		public static void Add(ConsoleCommand consoleCommand)
		{
			ConsoleCommands.Add(consoleCommand);
		}

		public static void Update()
		{
			if (KeyPresses.WasKeyPressed(Keys.Y))
				foreach (ConsoleCommand consoleCommand in ConsoleCommands)
				{
					consoleCommand();
				}
		}
	}
}
