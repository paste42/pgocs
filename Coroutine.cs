﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace PGOCS
{
	public class Coroutine
	{
		public float Wait;
		public bool Ended = true;
		private Stack<IEnumerator> _routines;

		public void Start(IEnumerator routine)
		{
			_routines = new Stack<IEnumerator>();
			_routines.Push(routine);
			Ended = false;
			Wait = 0f;
		}

		public bool IsEmpty { get { return Ended; } }

		public void Update(GameTime gameTime)
		{
			Wait -= gameTime.ElapsedGameTime.Milliseconds;
			if (Wait > 0) return;
			Step();
		}

		private void Step()
		{
			if (Ended) return;
			IEnumerator now = _routines.Peek();
			if (now.MoveNext())
			{
				if (now.Current is int)
					Wait = (int) now.Current;
				else if (now.Current is float)
					Wait = (float) now.Current;
				else
				{
					IEnumerator item = now.Current as IEnumerator;
					if (item != null)
					{
						_routines.Push(item);
					}
					else Wait = 0;
				}
			}
			else
			{
				_routines.Pop();
				if (_routines.Count == 0)
					Ended = true;
				else Step();
			}
		}
	}
}
