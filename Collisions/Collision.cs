﻿using Microsoft.Xna.Framework;
using PGOCS.Components;
using static PGOCS.Collisions.CollisionSystem;

namespace PGOCS.Collisions
{
    public class Collision
    {
        public Collider PrimaryCollider { get; private set; }
        public Collider SecondaryCollider { get; private set; }
        public CollisionFlags PrimaryFlags { get; private set; }
        public CollisionFlags SecondaryFlags { get; private set; }
        public bool IsStale;

        public Collision(Collider primary, Collider secondary)
        {
            PrimaryCollider = primary;
            SecondaryCollider = secondary;
            IsStale = false;
        }

        public void AddFlag(Collider collider, CollisionFlags flag)
        {
            if (collider == PrimaryCollider) PrimaryFlags |= flag;
            else if (collider == SecondaryCollider) SecondaryFlags |= flag;
            else throw new System.Exception("collider not found");
        }
    }
}
