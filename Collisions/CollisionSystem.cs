﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using PGOCS.Components;
using PGOCS.Tilemaps;

namespace PGOCS.Collisions
{
	/// <summary>
	/// Checks Colliders from all GameObjects in a Scene against the Colliders from selected other GameObjects.
	/// If the Scene is a TiledScene, it checks GameObjects' Colliders against the Tilemap
	/// </summary>
	public class CollisionSystem
	{
		[Flags]
		public enum CollisionFlags
		{
			None = 0,
			Left = 1,
			Right = 2,
			Top = 4,
			Bottom = 8
		};

        static List<Collision> _collisions;
		internal List<CollisionGroup> CollisionGroups;
        List<CollisionGroup> MovingPlatformCollisionGroups;

		public CollisionSystem()
		{
			CollisionGroups = new List<CollisionGroup>();
            MovingPlatformCollisionGroups = new List<CollisionGroup>();
            _collisions = new List<Collision>();
		}

		public void Collide(GameObject mainObject, ICollection<GameObject> group2)
		{
			foreach (GameObject other in group2)
				Collide(mainObject, other);
		}

        /// <summary>
        /// Adds object2 to the collision group of object1 (actuall collision detection occurs later in CollideWithTilemap)
        /// </summary>
        /// <param name="object1"></param>
        /// <param name="object2"></param>
		public void Collide(GameObject object1, GameObject object2)
		{
			CollisionGroup collisionGroup;
            var GroupList = CollisionGroups;
            if (object2.GetComponent<Collider>().IsMovingPlatform)
                GroupList = MovingPlatformCollisionGroups;
			if (GroupList.Exists(g => g.Primary == object1))
				collisionGroup = GroupList.Find(g => g.Primary == object1);
			else
			{
				collisionGroup = new CollisionGroup(object1);
                GroupList.Add(collisionGroup);
			}
				
			if (!collisionGroup.Others.Contains(object2)) collisionGroup.AddCollidingObject(object2);

		}

		public void CollideWithTilemap(IEnumerable<GameObject> list, Tilemap tilemap)
		{
			foreach (GameObject go in list
				.Where(go => go.HasComponent<Collider>())
				.Where(go => go.HasComponent<PhysicalBody>()))
			{
				if (go.GetComponent<Collider>().IsActive) CollideWithTilemap(go, tilemap);
				if (go.ChildList.Count > 0) CollideWithTilemap(go.ChildList, tilemap);
			}
		}

        public void PreUpdate()
        {
            CleanUpCollisionGroups();
            CleanCollisionList();
        }

        public void UpdatePhysicalBodies(GameTime gameTime, ICollection<GameObject> goList)
		{
            // Update moving platforms first
            foreach (GameObject go in goList.Where(go => go.HasComponent<PhysicalBody>()).Where(go => go.GetComponent<Collider>().IsMovingPlatform))
            {
                GetNewVelocityPosition(go, gameTime.ElapsedGameTime.Milliseconds);
                if (go.ChildList.Count > 0) UpdatePhysicalBodies(gameTime, go.ChildList);
            }
            foreach (GameObject go in goList.Where(go => go.HasComponent<PhysicalBody>()))
			{
                if (go.HasComponent<Collider>() && go.GetComponent<Collider>().IsMovingPlatform) continue;
				GetNewVelocityPosition(go, gameTime.ElapsedGameTime.Milliseconds);
				if (go.ChildList.Count > 0) UpdatePhysicalBodies(gameTime, go.ChildList);
			}
		}

        public void CleanCollisionList()
        {
            _collisions.RemoveAll(x => x.IsStale);
            foreach (Collision c in _collisions) c.IsStale = true;
        }

        public void SetOnMovingPlatformStatus(ICollection<GameObject> goList)
        {
            foreach (GameObject go in goList.Where(go => go.HasComponent<PhysicalBody>()))
            {
                go.GetComponent<Collider>().IsOnMovingPlatform = null;
                foreach (GameObject platform in goList.Where(platform => platform.HasComponent<Collider>())
                    .Where(platform => platform.GetComponent<Collider>().IsMovingPlatform))
                {
                    var goCollider = go.GetComponent<Collider>();
                    var goCollPosition = goCollider.GetAbsoluteTransform().Position;
                    var platformCollider = platform.GetComponent<Collider>();
                    var platCollPosition = platformCollider.GetAbsoluteTransform().Position;
                    if ((Math.Abs(platCollPosition.Y - goCollPosition.Y - goCollider.Size.Y) < 1)
                        && (goCollPosition.X < platCollPosition.X + platformCollider.Size.X)
                        && (platCollPosition.X < goCollPosition.X + goCollider.Size.X))
                    {
                        goCollider.IsOnMovingPlatform = platformCollider;
                        go.Transform.SetAbsolutePosition(new Vector2(go.Transform.Position.X, platCollPosition.Y - goCollider.Size.Y - goCollider.Position.Y));
                    }
                }
            }
        }

		private static void GetNewVelocityPosition(GameObject go, int elapsedMilliseconds)
		{
			var body = go.GetComponent<PhysicalBody>();
			Vector2 newPosition = go.Transform.GetAbsoluteTransform().Position;
			Vector2 velocity = body.Velocity;

			Vector2 acceleration = body.Acceleration;
			float elapsedTimeInSeconds = elapsedMilliseconds / 1000f;
            float timeStep = 1 / 60.0f;
            const int maxPasses = 5;
            int passes = 0;

            var collider = go.GetComponent<Collider>();
            if (collider != null)
            {
                // Move with moving platform
                if (collider.IsOnMovingPlatform != null)
                    newPosition += collider.IsOnMovingPlatform.GameObject.Transform.Position - collider.IsOnMovingPlatform.GameObject.Transform.PreviousPosition;
                else // Set variables in preparation for collision detection
                    collider.IsOnGround = false;
                collider.CollisionFlag = CollisionFlags.None;
                collider.TileCollisionFlag = CollisionFlags.None;
            }

            while ((elapsedTimeInSeconds > 0.000001f) && (passes < maxPasses))
            {
                float deltaTime = Math.Min(timeStep, elapsedTimeInSeconds);
                // x component
                velocity.X = CalculateSpeed(velocity.X, acceleration.X, body.MaxVelocity.X);
                newPosition.X += velocity.X * deltaTime;

                // y component
                velocity.Y = CalculateSpeed(velocity.Y, acceleration.Y, body.MaxVelocity.Y);
                newPosition.Y += velocity.Y * deltaTime;
                elapsedTimeInSeconds -= deltaTime;
                passes++;
            }

            go.Transform.SetAbsolutePosition(newPosition);
            body.Velocity = velocity;

		}

        /// <summary>
        /// Takes the gameObject, checks it against the tilemap and the objects in its CollisionGroup (if it
        /// exists) and runs the relevant collision reaction
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="tilemap"></param>
        public void CollideWithTilemap(GameObject gameObject, Tilemap tilemap)
        {
            // TODO: Change this so positions are calculated by collider position
            var collider = gameObject.GetComponent<Collider>();
            var previousPosition = gameObject.Transform.GetAbsolutePreviousPosition();
            if (previousPosition == Vector2.Zero) return;
            var newPosition = gameObject.Transform.GetAbsoluteTransform().Position;
            Point startTile = new Point((int)(previousPosition.X + collider.Position.X) / tilemap.TileWidth,
                (int)(previousPosition.Y + collider.Position.Y) / tilemap.TileHeight);
            Point endTile = new Point((int)(newPosition.X + collider.Position.X) / tilemap.TileWidth,
                (int)((newPosition.Y + collider.Position.Y) / tilemap.TileHeight));
            Point startTileWithSize = new Point((int)(previousPosition.X + collider.Position.X + collider.Size.X - 1) / tilemap.TileWidth,
                (int)(previousPosition.Y + collider.Position.Y + collider.Size.Y - 1) / tilemap.TileHeight);
            Point endTileWithSize = new Point((int)Math.Ceiling((newPosition.X + collider.Position.X + collider.Size.X) / (double)tilemap.TileWidth) - 1,
                (int)(newPosition.Y + collider.Position.Y + collider.Size.Y) / tilemap.TileHeight);
            CheckHorizontalTileCollisions(gameObject, startTile, startTileWithSize, endTile, endTileWithSize,
                collider, tilemap);

            newPosition = gameObject.Transform.GetAbsoluteTransform().Position;
            endTile = new Point((int)(newPosition.X + collider.Position.X) / tilemap.TileWidth, endTile.Y);
            endTileWithSize = new Point((int)Math.Ceiling((newPosition.X + collider.Position.X + collider.Size.X) / (double)tilemap.TileWidth) - 1,
                endTileWithSize.Y);
            CheckVerticalTileCollisions(gameObject, startTile, startTileWithSize, endTile, endTileWithSize,
                collider, tilemap);

            if (!CollisionGroups.Exists(g => g.Primary == gameObject)) return;
            startTile = new Point(Math.Min(startTile.X, endTile.X), Math.Min(startTile.Y, endTile.Y));
            endTile = new Point(Math.Max(startTileWithSize.X, endTileWithSize.X), Math.Max(startTileWithSize.Y, endTileWithSize.Y));
            CheckFreshlyActiveColliders(gameObject, collider, startTile, endTile);
        }

        public void CheckHorizontalTileCollisions(GameObject gameObject, Point startTile, Point startTileWithSize,
			Point endTile, Point endTileWithSize, Collider collider, Tilemap tilemap)
		{
            // TODO: Separate these loops so start and end tiles are reevaluated between them if collisions are found
			int i = -1;
			int j = -1;
			bool tileFlag = false;
			bool checkGroup = CollisionGroups.Exists(g => g.Primary == gameObject);
			CollisionGroup collisionGroup = null;
            CollisionGroup movingPlatformGroup = null;
            Point collidingTile = Point.Zero;
            if (checkGroup)
            {
                collisionGroup = CollisionGroups.Find(g => g.Primary == gameObject);
                movingPlatformGroup = MovingPlatformCollisionGroups.Find(g => g.Primary == gameObject);
            }
            // Moving left
            if (gameObject.Transform.GetAbsoluteTransform().Position.X < gameObject.Transform.GetAbsolutePreviousPosition().X)
            {
                // Check against moving platforms
                if (checkGroup && movingPlatformGroup != null)
                    for (i = startTile.X; i >= endTile.X; i--)
                        for (j = startTile.Y; j <= startTileWithSize.Y; ++j)
                            if (CollideWithCollisionGroupAtTileCoordsX(movingPlatformGroup, new Point(i, j), -1, tilemap))
                                break;
                // Check against tiles
                for (i = startTile.X; i >= endTile.X; i--)
                {
                    for (j = startTile.Y; j <= startTileWithSize.Y; ++j)
                        if ((tilemap.GetTile(i, j) != null) && (tilemap.GetTile(i, j).Status == Tile.TileStatus.Solid))
                        {
                            if ((gameObject.Transform.Position.Y + collider.Position.Y + collider.Size.Y - collider.HorizontalTolerance < tilemap.TileHeight * j) 
                                || (gameObject.Transform.Position.Y + collider.Position.Y + collider.HorizontalTolerance > tilemap.TileHeight * (j + 1)))
                                continue;
                            collidingTile = new Point(i, j);
                            tileFlag = true;
                            collider.TileCollisionFlag |= CollisionFlags.Left;
                            if ((collider.TileCollisionReaction == null) || (collider.UseDefaultAndCustomReaction))
                                SeparateHorizontallyFromTile(gameObject, collidingTile, tilemap.TileWidth, true);
                            if (collider.TileCollisionReaction != null) collider.TileCollisionReaction.Invoke();
                            break;
                        }
                    if (tileFlag) break;
                }
                // Check against collisiongroup
                if (checkGroup && collisionGroup != null)
                    for (i = startTile.X; i >= endTile.X; i--)
                        for (j = startTile.Y; j <= startTileWithSize.Y; ++j)
                            if (CollideWithCollisionGroupAtTileCoordsX(collisionGroup, new Point(i, j), -1, tilemap))
                                break;
            }
            // Moving right
            else if (gameObject.Transform.GetAbsoluteTransform().Position.X > gameObject.Transform.GetAbsolutePreviousPosition().X)
            {
                // Check against moving platforms
                if (checkGroup && movingPlatformGroup != null)
                    for (i = startTileWithSize.X; i <= endTileWithSize.X; ++i)
                        for (j = startTile.Y; j <= startTileWithSize.Y; ++j)
                            if (CollideWithCollisionGroupAtTileCoordsX(movingPlatformGroup, new Point(i, j), 1, tilemap))
                                break;
                // Check against tiles
                for (i = startTileWithSize.X; i <= endTileWithSize.X; ++i)
                {
                    for (j = startTile.Y; j <= startTileWithSize.Y; ++j)
                        if ((tilemap.GetTile(i, j) != null) && (tilemap.GetTile(i, j).Status == Tile.TileStatus.Solid))
                        {
                            if ((gameObject.Transform.Position.Y + collider.Position.Y + collider.Size.Y - collider.HorizontalTolerance < tilemap.TileHeight * j)
                                || (gameObject.Transform.Position.Y + collider.Position.Y + collider.HorizontalTolerance > tilemap.TileHeight * (j + 1)))
                                continue;
                            collidingTile = new Point(i, j);
                            collider.TileCollisionFlag |= CollisionFlags.Right;
                            tileFlag = true;
                            if ((collider.TileCollisionReaction == null) || (collider.UseDefaultAndCustomReaction))
                                SeparateHorizontallyFromTile(gameObject, collidingTile, tilemap.TileWidth, false);
                            if (collider.TileCollisionReaction != null) collider.TileCollisionReaction.Invoke();
                            break;
                        }
                    if (tileFlag) break;
                }
                // Check against collisiongroup
                if (checkGroup && collisionGroup != null)
                    for (i = startTileWithSize.X; i <= endTileWithSize.X; ++i)
                        for (j = startTile.Y; j <= startTileWithSize.Y; ++j)
                            if (CollideWithCollisionGroupAtTileCoordsX(collisionGroup, new Point(i, j), 1, tilemap))
                                break;
            }
		}

		public void CheckVerticalTileCollisions(GameObject gameObject, Point startTile, Point startTileWithSize,
			Point endTile, Point endTileWithSize, Collider collider, Tilemap tilemap)
		{
			int i = -1;
			int j = -1;
            int j2 = -1;
            int endTileY = endTileWithSize.Y;
			bool tileFlag = false;
            bool platformFlag = false;
			bool checkGroup = CollisionGroups.Exists(g => g.Primary == gameObject);
			CollisionGroup collisionGroup = null;
            CollisionGroup movingPlatformGroup = null;
            Point collidingTile = Point.Zero;
            if (checkGroup)
            {
                collisionGroup = CollisionGroups.Find(g => g.Primary == gameObject);
                movingPlatformGroup = MovingPlatformCollisionGroups.Find(g => g.Primary == gameObject);
            }
            // Moving down
            if (gameObject.Transform.GetAbsoluteTransform().Position.Y > gameObject.Transform.GetAbsolutePreviousPosition().Y)
            {
                // Check against moving platforms
                if (checkGroup && movingPlatformGroup != null)
                    for (j = startTileWithSize.Y; j <= endTileWithSize.Y; ++j)
                    {
                        for (i = endTile.X; i <= endTileWithSize.X; ++i)
                            if (checkGroup && CollideWithCollisionGroupAtTileCoordsY(movingPlatformGroup, new Point(i, j), 1, tilemap))
                            {
                                platformFlag = true;
                                endTileY = j;
                                break;
                            }
                        if (platformFlag) break;
                    }
                // Check against tiles
                for (j2 = startTileWithSize.Y; j2 <= endTileY; ++j2)// <= endTileWithSize.Y; ++j)
                {
                    for (i = endTile.X; i <= endTileWithSize.X; ++i)
                        if ((tilemap.GetTile(i, j2) != null) && (tilemap.GetTile(i, j2).Status == Tile.TileStatus.Solid))
                        {
                            collidingTile = new Point(i, j2);
                            tileFlag = true;
                            collider.TileCollisionFlag |= CollisionFlags.Bottom;
                            if ((collider.TileCollisionReaction == null) || (collider.UseDefaultAndCustomReaction))
                                SeparateVerticallyFromTile(gameObject, collidingTile, tilemap.TileWidth, true);
                            if (collider.TileCollisionReaction != null) collider.TileCollisionReaction.Invoke();
                            break;
                        }
                    if (tileFlag) break;
                }
                // Check against collisiongroup
                if (checkGroup && collisionGroup != null)
                    for (j = startTileWithSize.Y; j <= endTileWithSize.Y; ++j)
                        for (i = endTile.X; i <= endTileWithSize.X; ++i)
                            if (checkGroup && CollideWithCollisionGroupAtTileCoordsY(collisionGroup, new Point(i, j), 1, tilemap))
                                break;
            }
            // Moving up
            else if (gameObject.Transform.GetAbsoluteTransform().Position.Y < gameObject.Transform.GetAbsolutePreviousPosition().Y)
            {
                // Check against moving platforms
                if (checkGroup && movingPlatformGroup != null)
                    for (j = startTile.Y; j >= endTile.Y; --j)
                    {
                        for (i = endTile.X; i <= endTileWithSize.X; ++i)
                            if (checkGroup && CollideWithCollisionGroupAtTileCoordsY(movingPlatformGroup, new Point(i, j), -1, tilemap))
                            {
                                platformFlag = true;
                                break;
                            }
                        if (platformFlag) break;
                    }
                // Check against tiles
                for (j = startTile.Y; j >= endTile.Y; --j)
                {
                    for (i = endTile.X; i <= endTileWithSize.X; ++i)
                        if ((tilemap.GetTile(i, j) != null) && (tilemap.GetTile(i, j).Status == Tile.TileStatus.Solid))
                        {
                            collidingTile = new Point(i, j);
                            tileFlag = true;
                            collider.TileCollisionFlag |= CollisionFlags.Top;
                            if ((collider.TileCollisionReaction == null) || (collider.UseDefaultAndCustomReaction))
                                SeparateVerticallyFromTile(gameObject, collidingTile, tilemap.TileWidth, false);
                            if (collider.TileCollisionReaction != null) collider.TileCollisionReaction.Invoke();
                            break;
                        }
                    if (tileFlag) break;
                }
                // Check against collisiongroup
                if (checkGroup && collisionGroup != null)
                    for (j = startTile.Y; j >= endTile.Y; --j)
                        for (i = endTile.X; i <= endTileWithSize.X; ++i)
                            if (checkGroup && CollideWithCollisionGroupAtTileCoordsY(collisionGroup, new Point(i, j), -1, tilemap))
                                break;
            }
		}

        void CheckFreshlyActiveColliders(GameObject gameObject, Collider collider, Point startTile, Point endTile)
        {
            // TODO: maybe: change this so it checks collisions along the gameobject's path
            CollisionGroup collisionGroup = CollisionGroups.Find(g => g.Primary == gameObject);
            foreach (GameObject go in collisionGroup.Others.Where(x => x.GetComponent<Collider>().JustActivated))
                if (collider.CheckSimpleCollision(go.GetComponent<Collider>()))
                {
                    var collider2 = go.GetComponent<Collider>();
                    if (!collider.CollisionList.Contains(collider2.GameObject)) collider.CollisionList.Add(collider2.GameObject);
                    if (!collider2.CollisionList.Contains(collider.GameObject)) collider2.CollisionList.Add(collider.GameObject);
                    var collision = AddCollision(collider, collider2, CollisionFlags.None, CollisionFlags.None);
                    if (collider.CollisionReaction != null) collider.CollisionReaction.Invoke(collider2, collision);
                    if (collider2.CollisionReaction != null) collider2.CollisionReaction.Invoke(collider, collision);
                }
        }

        internal bool CollideWithCollisionGroupAtTileCoordsX(CollisionGroup collisionGroup, Point coords, int direction, Tilemap tilemap)
        {
            if (collisionGroup.Others.Count == 0) return false;
            bool result = false;
            Vector2 previousPrimaryPosition = collisionGroup.Primary.Transform.GetAbsolutePreviousPosition();
            Vector2 primaryPosition = collisionGroup.Primary.Transform.GetAbsoluteTransform().Position;
            Collider primaryCollider = collisionGroup.Primary.GetComponent<Collider>();
            foreach (GameObject go in collisionGroup.Others.Where(x => x.GetComponent<Collider>().IsActive))
            {
                var collider = go.GetComponent<Collider>();
                var position = collider.GetAbsoluteTransform().Position;
                if ((position.X + collider.Size.X > coords.X * tilemap.TileWidth) && (position.X < (coords.X + 1) * tilemap.TileWidth))
                {
                    if (previousPrimaryPosition.Y < primaryPosition.Y)
                    {
                        if ((previousPrimaryPosition.Y + primaryCollider.Position.Y + primaryCollider.HorizontalTolerance > position.Y + collider.Size.Y) ||
                            (primaryPosition.Y + primaryCollider.Position.Y + primaryCollider.Size.Y - primaryCollider.HorizontalTolerance <= position.Y)) continue;
                        result |= GetOverlapX(collisionGroup.Primary, go);
                    }
                    else
                    {
                        if ((primaryPosition.Y + primaryCollider.Position.Y + primaryCollider.HorizontalTolerance >= position.Y + collider.Size.Y) ||
                            (previousPrimaryPosition.Y + primaryCollider.Position.Y + primaryCollider.Size.Y - primaryCollider.HorizontalTolerance <= position.Y)) continue;
                        result |= GetOverlapX(collisionGroup.Primary, go);
                    }
                }
            }
            return result;
        }

        internal bool CollideWithCollisionGroupAtTileCoordsY(CollisionGroup collisionGroup, Point coords, int direction, Tilemap tilemap)
		{
			if (collisionGroup.Others.Count == 0) return false;
			bool result = false;
			Vector2 previousPrimaryPosition = collisionGroup.Primary.Transform.GetAbsolutePreviousPosition();
			Vector2 primaryPosition = collisionGroup.Primary.Transform.GetAbsoluteTransform().Position;
			Collider primaryCollider = collisionGroup.Primary.GetComponent<Collider>();
			foreach (GameObject go in collisionGroup.Others.Where(x => x.GetComponent<Collider>().IsActive))
			{
				var collider = go.GetComponent<Collider>();
				var position = collider.GetAbsoluteTransform().Position;
				if ((position.Y + collider.Size.Y > coords.Y * tilemap.TileHeight) && (position.Y < (coords.Y + 1) * tilemap.TileHeight)) 
				{
					if (previousPrimaryPosition.X < primaryPosition.X)
					{
						if ((previousPrimaryPosition.X + primaryCollider.Position.X > position.X + collider.Size.X) ||
							(primaryPosition.X + primaryCollider.Position.X + primaryCollider.Size.X <= position.X)) continue;
						result |= GetOverlapY(collisionGroup.Primary, go);
					}
					else
					{
						if ((primaryPosition.X + primaryCollider.Position.X >= position.X + collider.Size.X) ||
							(previousPrimaryPosition.X + primaryCollider.Position.X + primaryCollider.Size.X <= position.X)) continue;
						result |= GetOverlapY(collisionGroup.Primary, go);
					}
				}
			}
			return result;
		}

        /// <summary>
        /// Moves a GameObject along the horizontal axis so it no longer collides with a tile
        /// </summary>
        /// <param name="gameObject">The GameObject to be moved</param>
        /// <param name="tilePosition">The position of the tile to stop colliding with</param>
        /// <param name="tileSize"></param>
        /// <param name="moveRight">If true, move the GameObject to the right away from the tile, otherwise left</param>
		public static void SeparateHorizontallyFromTile(GameObject gameObject, Point tilePosition, int tileSize, bool moveRight)
		{
			float newPositionX;
			var goCollider = gameObject.GetComponent<Collider>();
			if (moveRight)
				newPositionX = (tilePosition.X + 1) * tileSize - goCollider.Position.X;
			else
				newPositionX = tilePosition.X * tileSize - goCollider.Size.X - goCollider.Position.X;
			gameObject.Transform.SetAbsolutePosition(new Vector2(newPositionX, gameObject.Transform.GetAbsoluteTransform().Position.Y));
		}

        /// <summary>
        /// Moves a GameObject along the vertical axis so it no longer collides with a tile
        /// </summary>
        /// <param name="gameObject">The GameObject to be moved</param>
        /// <param name="tilePosition">The position of the tile to stop colliding with</param>
        /// <param name="tileSize"></param>
        /// <param name="moveUp">If true, move the GameObject up away from the tile, otherwise down</param>
		public static void SeparateVerticallyFromTile(GameObject gameObject, Point tilePosition, int tileSize, bool moveUp)
		{
			float newPositionY;
			var goCollider = gameObject.GetComponent<Collider>();
			if (!moveUp)
				newPositionY = (tilePosition.Y + 1) * tileSize - goCollider.Position.Y;
			else
			{
				newPositionY = tilePosition.Y * tileSize - goCollider.Size.Y - goCollider.Position.Y;
				goCollider.IsOnGround = true;
				if (gameObject.HasComponent<PhysicalBody>()) gameObject.GetComponent<PhysicalBody>().SetVelocityY(0f);
			}
			gameObject.Transform.SetAbsolutePosition(new Vector2(gameObject.Transform.GetAbsoluteTransform().Position.X, newPositionY));
		}

		private bool GetOverlapX(GameObject go1, GameObject go2)
		{
			var collider1 = go1.GetComponent<Collider>();
			var collider2 = go2.GetComponent<Collider>();
            bool separateRight1;
            bool separateRight2;
			var position1 = collider1.GetAbsoluteTransform().Position.X;
			var delta1 = position1 - (go1.Transform.GetAbsolutePreviousPosition().X + collider1.Position.X);
			var position2 = collider2.GetAbsoluteTransform().Position.X;
			var delta2 = position2 - (go2.Transform.GetAbsolutePreviousPosition().X + collider2.Position.X);
			Rectangle r1 = new Rectangle(0, (int)(go1.Transform.GetAbsoluteTransform().Position.Y + collider1.Position.Y), 1, (int)collider1.Size.Y);
			Rectangle r2 = new Rectangle(0, (int)(go2.Transform.GetAbsoluteTransform().Position.Y + collider2.Position.Y), 1, (int)collider2.Size.Y);
			if (!(r1.Intersects(r2))) return false;

			float overlap;
            CollisionFlags newCollisionFlag1;
            CollisionFlags newCollisionFlag2;
            if (delta1 > delta2)
            {
                overlap = position1 + collider1.Size.X - position2;
                if (overlap <= 0) return false;
                if (overlap > Math.Abs(delta1) + Math.Abs(delta2))
                {
                    MaintainCollision(collider1, collider2);
                    return false;
                }
                collider1.CollisionFlag |= CollisionFlags.Right;
                newCollisionFlag1 = CollisionFlags.Right;
                separateRight1 = false;
                collider2.CollisionFlag |= CollisionFlags.Left;
                newCollisionFlag2 = CollisionFlags.Left;
                separateRight2 = true;
            }
            else
            {
                overlap = position2 + collider2.Size.X - position1;
                if (overlap <= 0) return false;
                if (overlap > Math.Abs(delta1) + Math.Abs(delta2))
                {
                    MaintainCollision(collider1, collider2);
                    return false;
                }
                collider1.CollisionFlag |= CollisionFlags.Left;
                newCollisionFlag1 = CollisionFlags.Left;
                separateRight1 = true;
                collider2.CollisionFlag |= CollisionFlags.Right;
                newCollisionFlag2 = CollisionFlags.Right;
                separateRight2 = false;
            }
            if (!collider1.CollisionList.Contains(collider2.GameObject)) collider1.CollisionList.Add(collider2.GameObject);
            if (!collider2.CollisionList.Contains(collider1.GameObject)) collider2.CollisionList.Add(collider1.GameObject);
            bool separate1 = collider1.IsMovable;
            bool separate2 = collider2.IsMovable;
            var collision = AddCollision(collider1, collider2, newCollisionFlag1, newCollisionFlag2);
            if (collider1.CollisionReaction != null) separate1 = collider1.CollisionReaction.Invoke(collider2, collision);
			if (collider2.CollisionReaction != null) separate2 = collider2.CollisionReaction.Invoke(collider1, collision);
            if (!collider1.IsSolid || !collider2.IsSolid)
				return true;
			if ((!separate1 || (delta1 == 0)) && separate2)
			{
				SeparateXBy(go2, overlap, separateRight2);
				go2.GetComponent<PhysicalBody>().SetVelocityX(0f);
			}
			else if ((!separate2 || (delta2 == 0)) && separate1)
			{
				SeparateXBy(go1, overlap, separateRight1);
				go1.GetComponent<PhysicalBody>().SetVelocityX(0f);
			}
			else if (separate1 && separate2)
			{
				SeparateXBy(go1, overlap / 2f, separateRight1);
				SeparateXBy(go2, overlap / 2f, separateRight2);
				go1.GetComponent<PhysicalBody>().SetVelocityX(0f);
				go2.GetComponent<PhysicalBody>().SetVelocityX(0f);
			}
            return true;
		}

		private void SeparateXBy(GameObject go, float overlap, bool separateRight)
		{
			var position = go.Transform.GetAbsoluteTransform().Position;
            var separateSign = separateRight ? 1 : -1;
			go.Transform.SetAbsolutePosition(new Vector2(position.X + separateSign * overlap, position.Y));
		}

		private bool GetOverlapY(GameObject go1, GameObject go2)
		{
			var collider1 = go1.GetComponent<Collider>();
			var collider2 = go2.GetComponent<Collider>();
            bool separateUp1;
            bool separateUp2;
			var position1 = collider1.GetAbsoluteTransform().Position.Y;
			var delta1 = position1 - (go1.Transform.GetAbsolutePreviousPosition().Y + collider1.Position.Y);
			var position2 = collider2.GetAbsoluteTransform().Position.Y;
			var delta2 = position2 - (go2.Transform.GetAbsolutePreviousPosition().Y + collider2.Position.Y);
			float overlap;
            CollisionFlags newCollisionFlag1;
            CollisionFlags newCollisionFlag2;
            if (delta1 > delta2)
            {
                overlap = position1 + collider1.Size.Y - position2;
                if (overlap > Math.Abs(delta1) + Math.Abs(delta2) + collider1.HorizontalTolerance + collider2.HorizontalTolerance)
                {
                    MaintainCollision(collider1, collider2);
                    return false;
                }
                if (overlap <= 0) return false;
                collider1.CollisionFlag |= CollisionFlags.Bottom;
                newCollisionFlag1 = CollisionFlags.Bottom;
                separateUp1 = true;
                collider2.CollisionFlag |= CollisionFlags.Top;
                newCollisionFlag2 = CollisionFlags.Top;
                separateUp2 = false;
            }
            else
            {
                overlap = position2 + collider2.Size.Y - position1;
                if (overlap > Math.Abs(delta1) + Math.Abs(delta2) + collider1.HorizontalTolerance + collider2.HorizontalTolerance)
                {
                    MaintainCollision(collider1, collider2);
                    return false;
                }
                if (overlap <= 0) return false;
                collider1.CollisionFlag |= CollisionFlags.Top;
                newCollisionFlag1 = CollisionFlags.Top;
                separateUp1 = false;
                collider2.CollisionFlag |= CollisionFlags.Bottom;
                newCollisionFlag2 = CollisionFlags.Bottom;
                separateUp2 = true;
            }
            if(!collider1.CollisionList.Contains(collider2.GameObject)) collider1.CollisionList.Add(collider2.GameObject);
            if (!collider2.CollisionList.Contains(collider1.GameObject)) collider2.CollisionList.Add(collider1.GameObject);
            bool separate1 = collider1.IsMovable;
            bool separate2 = collider2.IsMovable;
            var collision = AddCollision(collider1, collider2, newCollisionFlag1, newCollisionFlag2);
            if (collider1.CollisionReaction != null) separate1 = collider1.CollisionReaction.Invoke(collider2, collision);
			if (collider2.CollisionReaction != null) separate2 = collider2.CollisionReaction.Invoke(collider1, collision);
            if (!collider1.IsSolid || !collider2.IsSolid)
				return true;
			if ((!separate1 || (delta1 == 0)) && separate2)
			{
				SeparateYBy(go2, overlap, separateUp2);
				go2.GetComponent<PhysicalBody>().SetVelocityY(0f);
				if (delta2 > 0) collider2.IsOnGround = true;
			}
			else if ((!separate2 || (delta2 == 0)) && separate1)
			{
				SeparateYBy(go1, overlap, separateUp1);
				go1.GetComponent<PhysicalBody>().SetVelocityY(0f);
				if (delta1 > 0) collider1.IsOnGround = true;
			}
			else if (separate1 && separate2)
			{
				SeparateYBy(go1, overlap / 2f, separateUp1);
				SeparateYBy(go2, overlap / 2f, separateUp2);
				go1.GetComponent<PhysicalBody>().SetVelocityY(0f);
				go2.GetComponent<PhysicalBody>().SetVelocityY(0f);
				if (delta1 > 0) collider1.IsOnGround = true;
				else if (delta2 > 0) collider2.IsOnGround = true;
			}
            if (collider2.IsMovingPlatform) collider1.IsOnMovingPlatform = collider2;
			return true;
		}

		private void SeparateYBy(GameObject go, float overlap, bool separateUp)
		{
			var position = go.Transform.GetAbsoluteTransform().Position;
            var separateDirection = separateUp ? 1 : -1;
			go.Transform.SetAbsolutePosition(new Vector2(position.X, position.Y - separateDirection * overlap));
		}

        private Collision AddCollision(Collider collider1, Collider collider2, CollisionFlags flag1, CollisionFlags flag2)
        {
            // TODO: verify this doesn't need to check for c1 and c2 swapped
            var collision = new Collision(collider1, collider2);
            var oldCollision = _collisions.Find(x => x.PrimaryCollider == collider1 && x.SecondaryCollider == collider2);
            if (oldCollision == null)
            {
                _collisions.Add(collision);
                collision.AddFlag(collider1, flag1);
                collision.AddFlag(collider2, flag2);
                return collision;
            }
            else
            {
                oldCollision.AddFlag(collider1, flag1);
                oldCollision.AddFlag(collider2, flag2);
                oldCollision.IsStale = false;
                return oldCollision;
            }
        }

        private void MaintainCollision(Collider collider1, Collider collider2)
        {
            // TODO: check to see if this needs to catch collisions where the colliders are swapped
            var collision = _collisions.Find(x => (x.PrimaryCollider == collider1) && (x.SecondaryCollider == collider2));
            if (collision != null)
            {
                collision.IsStale = false;
                if (collider1.StillCollidingReaction != null) collider1.StillCollidingReaction.Invoke(collider2);
                if (collider2.StillCollidingReaction != null) collider2.StillCollidingReaction.Invoke(collider1);
            }
        }

        public static Collision GetCollision(Collider primary, Collider secondary)
        {
            return _collisions.Find(x => (x.PrimaryCollider == primary) && (x.SecondaryCollider == secondary));
        }

		public bool AreOverlapping(GameObject go1, GameObject go2)
		{
			var collider1 = go1.GetComponent<Collider>();
			var collider2 = go2.GetComponent<Collider>();
			var position1 = go1.Transform.GetAbsoluteTransform().Position + collider1.Position;
			var position2 = go2.Transform.GetAbsoluteTransform().Position + collider2.Position;
			return (!((position1.X >= position2.X + collider2.Size.X) || (position2.X >= position1.X + collider1.Size.X)
			          || (position1.Y >= position2.Y + collider2.Size.Y) || (position2.Y >= position1.Y + collider1.Size.Y)));
		}

		public static bool AreOverlapping(GameObject go, Tilemap tilemap, Point tile)
		{
			var collider = go.GetComponent<Collider>();
			var rect1 = new Rectangle((int)go.Transform.GetAbsoluteTransform().Position.X,
				(int)go.Transform.GetAbsoluteTransform().Position.Y, (int)collider.Size.X, (int)collider.Size.Y);
			var rect2 = new Rectangle(tilemap.TileWidth * tile.X, tilemap.TileHeight * tile.Y, tilemap.TileWidth, tilemap.TileHeight);
			return rect1.Intersects(rect2);
		}
		private static float CalculateSpeed(float initialVelocity, float acceleration, float maxVelocity)
		{
			float finalVelocity = initialVelocity + acceleration;
			return Math.Sign(finalVelocity) * Math.Min(Math.Abs(finalVelocity), maxVelocity);
		}

		/// <summary>
		/// Returns a List of coordinates of solid Tiles that the parameter Rectangle overlaps or null if the
		/// rectangle is outside the map.
		/// </summary>
		/// <param name="rect">The rectangle to check against the tilemap, in screen coordinates</param>
		/// <param name="tilemap">The tilemap to check the rectangle against</param>
		/// <returns></returns>
		public static List<Point> CheckOverlappingTiles(Rectangle rect, Tilemap tilemap)
		{
			Point tilemapSize = new Point(tilemap.Width*tilemap.TileWidth, tilemap.Height*tilemap.TileHeight);
			if ((rect.Left > tilemapSize.X) || (rect.Top > tilemapSize.Y)
			    || (rect.Right < 0) || (rect.Bottom < 0)) return null;
			var tempList = new List<Point>();
			Point beginTile = tilemap.GetTileCoords(new Vector2(Math.Max(0, rect.X), Math.Max(0, rect.Y)));
			Point endTile = tilemap.GetTileCoords(new Vector2(Math.Min(rect.Right, tilemapSize.X), Math.Min(rect.Bottom, tilemapSize.Y)));
			for (int i = beginTile.X; i <= endTile.X; ++i)
				for (int j = beginTile.Y; j <= endTile.Y; ++j)
			{
				if (tilemap.GetTile(i, j).Status == Tile.TileStatus.Solid)
					tempList.Add(new Point(i, j));
			}
			return tempList.Count == 0 ? null : tempList;
		}

		public void CleanUpCollisionGroups()
		{
			CollisionGroups = new List<CollisionGroup>();
		}
	}
}
