﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGOCS.Collisions
{
	class CollisionGroup
	{
		public List<GameObject> Others { get; set; }
		public GameObject Primary { get; private set; }

		public CollisionGroup(GameObject primary)
		{
			Primary = primary;
			Others = new List<GameObject>();
		}

		public void AddCollidingObject(GameObject go)
		{
			Others.Add(go);
		}
	}
}
