﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PGOCS.Collisions
{
	internal static class ColliderTexture
    {
        public static Texture2D OverlayTex { get; private set; }
        public static bool IsVisible { get; private set; }
	    private static bool _isCreated;
        static ColliderTexture()
        {
            IsVisible = false;
            _isCreated = false;
        }

        public static void CreateTexture(IGame game)
        {
	        if (_isCreated) return;
	        OverlayTex = new Texture2D(game.GraphicsDevice, 1, 1);
	        OverlayTex.SetData(new[] { new Color(255, 0, 0, 125) });
	        _isCreated = true;
        }

        public static void ToggleVisible(IGame game)
        {
            if (!_isCreated) CreateTexture(game);
            IsVisible = !IsVisible;

        }
        
    }
}
