﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PGOCS
{
    public abstract class GOComponent
    {
        public GOComponent(string familyID)
        {
            this.familyID = familyID;
        }

        public string familyID { get; private set; }
        public string componentID { get; set; }

        public abstract void Update(GameTime gameTime);
    }
}
