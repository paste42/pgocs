﻿using Microsoft.Xna.Framework;

namespace PGOCS.Components
{
    public abstract class GOComponent
    {
	    public string ComponentId { get; set; }

        public GameObject GameObject { get; set; }

        public abstract void Update(GameTime gameTime);

		public virtual void LateUpdate(GameTime gameTime) { }
    }
}
