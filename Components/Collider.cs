﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PGOCS.Collisions;
using System;
using System.Collections.Generic;

namespace PGOCS.Components
{
    public class Collider : GOComponent
    {

		public CollisionSystem.CollisionFlags CollisionFlag { get; set; }
        public CollisionSystem.CollisionFlags TileCollisionFlag { get; set; }

        public Vector2 Position { get; set; }

	    public Vector2 Size { get; set; }
        
        /// <summary>
        /// Holds the method to invoke when a collision is found. Returns true when separating
        /// algorithm should also be called.
        /// </summary>
        public Func<Collider, Collision, bool> CollisionReaction;
        public Action<Collider> StillCollidingReaction;
		public Action TileCollisionReaction;
	    public bool UseDefaultAndCustomReaction = false;

        public List<GameObject> CollisionList;

        private bool _isActive;
        public bool JustActivated { get; private set; }
	    public bool IsOnGround { get; set; }
		public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (_isActive) JustActivated = true;
            }
        }
		public bool IsMovable { get; set; }
		public bool IsSolid { get; set; }
        public bool IsMovingPlatform { get; set; }
        public Collider IsOnMovingPlatform { get; set; }

        /// <summary>
        /// The amount of space to ignore for horizontal collisions. Collision detection will still register for vertical 
        /// collisions allowing for minor imperfections or blockages in horizontal movement.
        /// </summary>
        public readonly float HorizontalTolerance;

	    public Collider(Vector2 size, Vector2 position = new Vector2(), bool isMovable = true, bool isSolid = true, float horizontalTolerance = 0)
	    {
            Size = size;
            Position = position;
		    IsMovable = isMovable;
		    IsSolid = isSolid;
		    IsActive = true;
            IsOnGround = false;
            IsMovingPlatform = false;
            IsOnMovingPlatform = null;
		    TileCollisionReaction = null;
		    CollisionReaction = null;
            HorizontalTolerance = horizontalTolerance;
            CollisionList = new List<GameObject>();
	    }

        public override void Update(GameTime gameTime)
        {
            CollisionList.Clear();
        }

        public override void LateUpdate(GameTime gameTime)
        {
            JustActivated = false;
            base.LateUpdate(gameTime);
        }

        private void DefaultReactToHorizontalCollision(int tileSize, Point tilePosition, 
			ref Vector2 newPosition, ref Vector2 velocity)
	    {

	    }

		private void DefaultReactToVerticalCollision(int tileSize, Point tilePosition, 
			ref Vector2 newPosition, ref Vector2 velocity)
		{

		}

	    public bool CheckSimpleCollision(Collider other)
	    {
			Vector2 location = new Vector2(GameObject.Transform.GetAbsoluteTransform().Position.X + Position.X,
				GameObject.Transform.GetAbsoluteTransform().Position.Y + Position.Y);
			Vector2 otherLocation = new Vector2(other.GameObject.Transform.GetAbsoluteTransform().Position.X + other.Position.X,
				other.GameObject.Transform.GetAbsoluteTransform().Position.Y + other.Position.Y);
            bool collisionExists = (location.X + Size.X > otherLocation.X) && (location.X < otherLocation.X + other.Size.X)
                && (location.Y + Size.Y > otherLocation.Y) && (location.Y < otherLocation.Y + other.Size.Y);
            if (!collisionExists) return false;
            if (!CollisionList.Contains(other.GameObject)) CollisionList.Add(other.GameObject);
            if (!other.CollisionList.Contains(GameObject)) other.CollisionList.Add(GameObject);
            return true;
	    }

        public float GetOverlapX(Collider other)
        {
            float location = GameObject.Transform.GetAbsoluteTransform().Position.X + Position.X;
            float otherLocation = other.GameObject.Transform.GetAbsoluteTransform().Position.X + other.Position.X;
            if (location < otherLocation) return (location + Size.X) - otherLocation;
            else return Math.Max((otherLocation + other.Size.X) - location, 0);
        }

        public float GetOverlapY(Collider other)
        {
            float location = GameObject.Transform.GetAbsoluteTransform().Position.Y + Position.Y;
            float otherLocation = other.GameObject.Transform.GetAbsoluteTransform().Position.Y + other.Position.Y;
            if (location < otherLocation) return (location + Size.Y) - otherLocation;
            else return Math.Max((otherLocation + other.Size.Y) - location, 0);
        }

        public void Draw(SpriteBatch sb, Transform t, Camera c)
	    {
		    if (!IsActive) return;
            Transform screenTransform = c.GetScreenTransform(t);
			Transform newt = Transform.Compose(screenTransform, new Transform(new Vector2(Position.X, Position.Y)));
			sb.Draw(ColliderTexture.OverlayTex, newt.Position, null, Color.White, newt.Rotation, Vector2.Zero, new Vector2(Size.X, Size.Y), SpriteEffects.None, t.Depth);
        }

	    public Transform GetAbsoluteTransform()
	    {
		    return Transform.Compose(GameObject.Transform.GetAbsoluteTransform(), new Transform(Position));
	    }
    }
}
