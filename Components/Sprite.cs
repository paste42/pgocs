﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Remoting.Channels;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PGOCS.Components
{
    public class Sprite : GOComponent
    {
	    readonly Texture2D _texture;
        Point _frameSize;
        Point _sheetSize;
	    public Transform Transform;
        Dictionary<string, Animation> _animations;
        string _currentAnimation;
        public int CurrentFrame { get; set; }
        
        public bool IsFlippedHorizontally { get; set; }

	    public Sprite(IGame game, string texture, Point frameSize, Point sheetSize, Transform transform = null)
	    {
		    _texture = game.Content.Load<Texture2D>(texture);
            _frameSize = frameSize;
            _sheetSize = sheetSize;
            CurrentFrame = 0;
            _currentAnimation = null;
	        Transform = transform ?? new Transform();
            _animations = new Dictionary<string, Animation>();
            IsFlippedHorizontally = false;
            IsVisible = true;
        }

        public void Draw(SpriteBatch sb, Transform t, Camera c)
        {
            // TODO: implement scale?
	        if (!IsVisible) return;
	        Transform screenTransform = c.GetScreenTransform(Transform.Compose(t.GetAbsoluteTransform(), Transform));
	        SpriteEffects spriteEffects = SpriteEffects.None;
	        if (IsFlippedHorizontally) spriteEffects = SpriteEffects.FlipHorizontally;
	        sb.Draw(_texture, screenTransform.Position, GetFrameRectangle(), Color.White, 
		        screenTransform.Rotation, Vector2.Zero, 1f, spriteEffects, screenTransform.Depth);
        }

        private Rectangle? GetFrameRectangle()
        {
            int row = CurrentFrame / _sheetSize.X;
            int col = CurrentFrame % _sheetSize.X;
            return new Rectangle(col * _frameSize.X, row * _frameSize.Y, _frameSize.X, _frameSize.Y);
        }

        public void AddAnimation(string name, int[] frameSequence, int millisecondsPerFrame = 100)
        {
            _animations.Add(name, new Animation(frameSequence, millisecondsPerFrame));
            Debug.WriteLine("Added animation " + name + ": " + _animations[name]);
        }

        public void PlayAnimation(string name, bool looped = true, int startingFrame=0)
        {
            
            if (_currentAnimation != name)
            {
                StopAnimation();
                _currentAnimation = name;
            }
            if (!_animations[name].IsPlaying)
            {
                _animations[name].IsPlaying = true;
                _animations[name].IsLooping = looped;
                _animations[name].CurrentFrame = startingFrame;
            }
        }

        public bool IsPlayingAnimation()
        {
            return (_currentAnimation != null) && (_animations[_currentAnimation].IsPlaying);
        }


        public void StopAnimation()
        {
            if (_currentAnimation != null)
            {
                _animations[_currentAnimation].Stop();
                _currentAnimation = null;
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (_currentAnimation != null)
            {
                _animations[_currentAnimation].Update(gameTime);
                CurrentFrame = _animations[_currentAnimation].CurrentFrame;
            }
        }

        public void SetFrame(int frame)
        {
            CurrentFrame = frame;
        }

        public bool IsVisible { get; set; }

        public void FlipHorizontally()
        {
            IsFlippedHorizontally = !IsFlippedHorizontally;
        }
    }
}
