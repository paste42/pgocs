﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace PGOCS.Components
{
    public class Transform : GOComponent
    {
        /// <summary>
        /// List of child transforms
        /// </summary>
        List<Transform> _children;

        public Transform(Vector2 position = new Vector2(), float depth = 0f, float rotation = 0f)
        {
            Position = position;
            Depth = depth;
            Rotation = rotation;
            _children = new List<Transform>();
        }

        public override void Update(GameTime gameTime)
        {
            UpdatePrevious();
        }
        
        /// <summary>
        /// Combines two transforms' positions and rotations
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public static Transform Compose(Transform parent, Transform child)
        {
            Transform result = new Transform(parent.Position + child.Position, parent.Depth + child.Depth, parent.Rotation + child.Rotation);
            result.Rotation -= (float)Math.Floor(result.Rotation);
            if ((result.Depth < 0f) || (result.Depth > 1.0f))
            {
                Debug.WriteLine("Depth out of bounds");
                result.Depth = MathHelper.Clamp(result.Depth, 0f, 1.0f);
            }
			result.Parent = child.Parent ?? null;
            return result; 
        }
        
        public static Transform operator -(Transform t)
        {
            return new Transform(-t.Position, t.Depth, 1 - t.Rotation);
        }

        /// <summary>
        /// position in world space or relative to the parent
        /// </summary>
        public Vector2 Position { get; set; }
        /// <summary>
        /// the position in world space or relative to the parent in the last frame. it is automatically updated in LateUpdate and
        /// should only be set when correcting something from if Position was manually set
        /// </summary>
		public Vector2 PreviousPosition { get; set; }
        
        /// <summary>
        /// rotation around the z axis, clockwise in radians
        /// </summary>
        public float Rotation { get; set; }
		public float PreviousRotation { get; private set; }
        /// <summary>
        /// depth from 0 to 1, 0 being the absolute front and 1 being the absolute back
        /// </summary>
        public float Depth { get; set; }
		public float PreviousDepth { get; private set; }

        private Transform _myParent;
        /// <summary>
        /// The parent transform of this transform
        /// </summary>
        public Transform Parent
        {
            get { return _myParent; }
            set
            {
                if (value == null)
                {
					if (_myParent == null) return;
                    _myParent._children.Remove(this);
                    _myParent = null;
                }
                else
                {
                    _myParent = value;
                    _myParent._children.Add(this);
                }
            }
        }

        /// <summary>
        /// Returns the child Transform at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Transform GetChild(int index)
        {
            return _children[index];
        }

        public void AddChild(Transform child)
        {
            child.Parent = this;
        }

        public void RemoveChild(Transform child)
        {
            child.Parent = null;
        }

		/// <summary>
		/// Composes the hierarchy of Transforms to find the position of this Transform in the Scene
		/// </summary>
		/// <returns>A Transform with the coordinates of this Transform in the Scene</returns>
        public Transform GetAbsoluteTransform()
        {
	        return Parent != null ? Compose(Parent.GetAbsoluteTransform(), this) : this;
        }

	    public void SetAbsolutePosition(Vector2 absolutePosition)
	    {
		    if (Parent == null) Position = absolutePosition;
			else Parent.SetAbsolutePosition(absolutePosition - Position);
	    }

	    public Vector2 GetAbsolutePreviousPosition()
	    {
		    if (Parent == null) return PreviousPosition;
		    return Parent.GetAbsoluteTransform().PreviousPosition + PreviousPosition;
	    }

	    private void UpdatePrevious()
	    {
		    PreviousPosition = Position;
		    PreviousDepth = Depth;
		    PreviousRotation = Rotation;
	    }
    }
}
