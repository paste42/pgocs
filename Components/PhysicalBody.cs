﻿using Microsoft.Xna.Framework;

namespace PGOCS.Components
{
    /// <summary>
    /// Makes the containing GameObject subject to the physics of the Scene
    /// </summary>
    public class PhysicalBody : GOComponent
    {
        public double Mass { get; private set; }
        public Vector2 Acceleration { get; set; }
        public Vector2 Velocity { get; set; }
        public Vector2 MaxVelocity { get; set; }
		public Vector2 ResultPosition { get; set; }

        public PhysicalBody(Vector2? maxVelocity = null, double mass = 1d)
        {
            Mass = mass;
			MaxVelocity = maxVelocity ?? new Vector2(float.MaxValue, float.MaxValue);
            Acceleration = Vector2.Zero;
            Velocity = Vector2.Zero;
        }

        public void SetVelocityX(float x) { Velocity = new Vector2(x, Velocity.Y); }

        public void SetVelocityY(float y) { Velocity = new Vector2(Velocity.X, y); }
        public void SetAccelerationX(float x) { Acceleration = new Vector2(x, Acceleration.Y); }
        public void SetAccelerationY(float y) { Acceleration = new Vector2(Acceleration.X, y); }
        public void SetMaxVelocityX(float x) { MaxVelocity = new Vector2(x, MaxVelocity.Y); }

        public override void Update(GameTime gameTime)
        {
        }
    }
}
