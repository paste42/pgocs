﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PGOCS
{
    public static class Engine
    {
        static SpriteBatch spriteBatch;
        public static RenderTarget2D renderTarget;

        public static void Start(Game game)
        {
            spriteBatch = new SpriteBatch(game.GraphicsDevice);
            PresentationParameters pp = game.GraphicsDevice.PresentationParameters;
            renderTarget = new RenderTarget2D(game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, true,
                game.GraphicsDevice.DisplayMode.Format, DepthFormat.Depth24);

        }

        // TODO: plug Engine into Game or Game1
        public static void Draw(Game game)
        {
            game.GraphicsDevice.SetRenderTarget(renderTarget);
            
        }

        public static void PostDraw(Game game)
        {
            game.GraphicsDevice.SetRenderTarget(null);
            // TODO: replace the scale
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(2f));
            spriteBatch.Draw((Texture2D)renderTarget, Vector2.Zero, Color.White);
            spriteBatch.End();

        }
    }
}
