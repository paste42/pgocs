﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGOCS;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PGOCS
{
    public class Sprite : GOComponent
    {
        Texture2D texture;
        Point frameSize;
        Point sheetSize;
        Dictionary<string, Animation> animations;
        string currentAnimation;
        int currentFrame;

        /// <summary>
        /// The sprite to be rendered for the GameObject it's attached to.
        /// </summary>
        /// <param name="texture">The spritesheet.</param>
        /// <param name="frameSize">The size in pixels of one frame of the sheet.</param>
        /// <param name="sheetSize">The size in frames of the spritesheet.</param>
        public Sprite(Texture2D texture, Point frameSize, Point sheetSize) : base("Sprite")
        {
            this.texture = texture;
            this.frameSize = frameSize;
            this.sheetSize = sheetSize;
            currentFrame = 0;
            currentAnimation = null;
            animations = new Dictionary<string, Animation>();
        }

        public void Draw(SpriteBatch sb, Transform t, Camera c)
        {
            // TODO: implement scale?
            Transform screenTransform = c.GetScreenTransform(t);
            sb.Draw(texture, screenTransform.Position, GetFrameRectangle(), Color.White, screenTransform.Rotation, Vector2.Zero, 1f, SpriteEffects.None, t.Depth);
        }

        private Rectangle? GetFrameRectangle()
        {
            int row = currentFrame / sheetSize.X;
            int col = currentFrame % sheetSize.X;
            return new Rectangle(col * frameSize.X, row * frameSize.Y, frameSize.X, frameSize.Y);
        }

        public void Dispose()
        {
            // TODO: check to make sure this needs no implementation
        }

        public void AddAnimation(string name, int[] frameSequence, int millisecondsPerFrame = 100)
        {
            animations.Add(name, new Animation(frameSequence, millisecondsPerFrame));
            System.Diagnostics.Debug.WriteLine("Added animation " + name + ": " + animations[name].ToString());
        }

        public void PlayAnimation(string name, bool looped = true, int startingFrame=0)
        {
            
            if (currentAnimation != name)
            {
                StopAnimation();
                currentAnimation = name;
                
            }
            if (!animations[name].IsPlaying)
            {
                animations[name].IsPlaying = true;
                animations[name].CurrentFrame = startingFrame;
            }
        }



        public void StopAnimation()
        {
            if (currentAnimation != null)
                animations[currentAnimation].Stop();
        }

        public override void Update(GameTime gameTime)
        {
            if (currentAnimation != null)
            {
                animations[currentAnimation].Update(gameTime);
                currentFrame = animations[currentAnimation].CurrentFrame;
            }
        }
    }
}
