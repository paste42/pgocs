﻿namespace PGOCS.Scenes
{
	public interface IPausable
	{
		bool IsPaused { get; set; }
		void Pause();
	}
}
