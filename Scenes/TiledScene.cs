﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PGOCS.Components;
using PGOCS.Content_Building;
using PGOCS.Input;
using PGOCS.Tilemaps;
using TiledPipelineExtLib;
using TiledSharp;

namespace PGOCS.Scenes
{
    public abstract class TiledScene : Scene
    {
	    readonly string _mapFilename;
        public Tilemap Tilemap;
        protected string TileTextureFilename;
        FileSystemWatcher _fileSystemWatcher;
        ContentManager _levelContentManager;
        ContentBuilder _contentBuilder;
        bool _isGridOn;
	    private static bool _usePreloadedMaps = true;

        // debug texture
        Texture2D _gridline; 

        Texture2D _tileMapTexture;

        public float Gravity { get; set; }

        /// <summary>
        /// Create a new TiledScene
        /// </summary>
        /// <param name="game"></param>
        /// <param name="tileTextureFilename">The filename of the image containing the textures of the tiles</param>
        /// <param name="mapFilename">Leave blank to load no pre-made map</param>
        /// <param name="scale"></param>
        protected TiledScene(IGame game, string tileTextureFilename, string mapFilename = "", float scale = 1f)
            : base(game, scale)
        {
            _mapFilename = mapFilename;
            TileTextureFilename = tileTextureFilename;
            Gravity = 0f;
            _isGridOn = false;
            SetupGrid();
        }

        public void SetupGrid()
        {
            _gridline = new Texture2D(Game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            int[] pixel = { 0xFFFFFF };
            _gridline.SetData(pixel);
        }

        public override void Initialize()
        {
			_levelContentManager = new ContentManager(Game.Content.ServiceProvider, "Content/");
	        if ((_mapFilename != "") && !_usePreloadedMaps)
	        {
				CheckFileChanges();
		        _contentBuilder = new ContentBuilder(Game.DirConfig.ConfigData.GameProjectDir);
	        }
	        base.Initialize();
        }

	    [Conditional("DEBUG")]
	    private void CheckFileChanges()
	    {
			_fileSystemWatcher = new FileSystemWatcher(Game.DirConfig.ConfigData.ContentProjectDir);
			_fileSystemWatcher.Filter = "*.*";
			_fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
			_fileSystemWatcher.Changed += OnFileChanged;
			_fileSystemWatcher.EnableRaisingEvents = true;
	    }

        private void OnFileChanged(object source, FileSystemEventArgs e)
        {
            if (_mapFilename != "")
            {
                _levelContentManager.Unload();
                _contentBuilder.Clear();
                _contentBuilder.Add(Game.DirConfig.ConfigData.ContentProjectDir + _mapFilename + ".tmx", _mapFilename, "TmxImporter", "TmxProcessor");
                Console.WriteLine("Content builder output: " + _contentBuilder.Build());
                LoadTilemap();
                Size = new Vector2(Tilemap.TileWidth * Tilemap.Width, Tilemap.TileHeight * Tilemap.Height);
            }
        }

        protected override void LoadContent()
        {
            if (_mapFilename != "") LoadTilemap();
            Size = new Vector2(Tilemap.TileWidth * Tilemap.Width, Tilemap.TileHeight * Tilemap.Height);

	        _tileMapTexture = _levelContentManager.Load<Texture2D>(TileTextureFilename);
        }

        /// <summary>
        /// Loads the tmxMap into the Tilemap. Override this function to put proper Tile states into 
        /// the tilemap.
        /// </summary>
        protected virtual void LoadTilemap()
        {
            // load the tmx map to a separate content manager for easy reloading
            TmxXml tmxXml = _levelContentManager.Load<TmxXml>(_mapFilename);
            TmxMap tmxMap = new TmxMap(Encoding.Default.GetString(tmxXml.BinaryMap), true);
            Tilemap = new Tilemap(tmxMap);
        }

        protected override void UnloadContent()
        {
            _levelContentManager.Unload();
        }

		/// <summary>
		/// Draws the TileMap
		/// </summary>
		/// <param name="camera">The camera to draw to. If null, draws to MainCamera</param>
        public void DrawTileMap(Camera camera)
        {
	        if (camera == null) camera = MainCamera;
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(1f));
            // All this xDecimal/positionModifier crap is because of a bug that occurs where if the camera position's decimal portion is in between
            // those values, it will shift the rectangle in the tile sheet over one pixel so the neighboring tile's first column
            // is displayed
            float xDecimal = camera.Transform.Position.X - (float)Math.Truncate(camera.Transform.Position.X);
            Vector2 positionModifier = Vector2.Zero;
            if (xDecimal > .49f)
            {
                if (xDecimal < .495f)
                    positionModifier = new Vector2(1 - .49f + xDecimal, 0);
                else if (xDecimal < .50)
                    positionModifier = new Vector2(1 - .5f + xDecimal, 0);
            }

            for (int i = 0; i < Tilemap.Height; ++i)
                for (int j = 0; j < Tilemap.Width; ++j)
                {
	                var currentTile = Tilemap.GetTile(j, i);
	                if (currentTile.Id > 0)
                    SpriteBatch.Draw(_tileMapTexture, 
						camera.GetScreenPosition(new Vector2(j * Tilemap.TileWidth, i * Tilemap.TileHeight)) + positionModifier,
                        new Rectangle((currentTile.Id - 1) * Tilemap.TileWidth, 0, Tilemap.TileWidth, Tilemap.TileHeight),
                        Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .5f);
                }
			SpriteBatch.End();
        }

        public void DrawGrid(Camera camera)
        {
            if (camera == null) camera = MainCamera;
            Vector2 adjustedPosition = camera.GetScreenPosition(Vector2.Zero);
            SpriteBatch.Begin();
            for(int i = 0; i < Tilemap.Width; ++i)
                SpriteBatch.Draw(_gridline, new Rectangle((int)adjustedPosition.X + i * Tilemap.TileWidth, (int)adjustedPosition.Y, 1, Tilemap.TileHeight * Tilemap.Height), Color.Pink);
            for (int j = 0; j < Tilemap.Height; ++j)
                SpriteBatch.Draw(_gridline, new Rectangle((int)adjustedPosition.X, (int)adjustedPosition.Y + j * Tilemap.TileHeight, Tilemap.TileWidth * Tilemap.Width, 1), Color.Pink);
            SpriteBatch.End();
        }

        public void PrintMap()
        {
            for (int i = 0; i < Tilemap.Height; ++i)
            {
                for (int j = 0; j < Tilemap.Width; ++j)
                    Console.Write(Tilemap.GetTile(j, i).Id);
                Console.WriteLine();
            }
        }

        public override void Update(GameTime gameTime)
        {
			CheckDebugInput();
			base.Update(gameTime);
        }

		[Conditional("DEBUG")]
	    private void CheckDebugInput()
	    {
			if (KeyPresses.WasKeyPressed(Keys.P)) PrintMap();
			if (KeyPresses.WasKeyPressed(Keys.G)) _isGridOn = !_isGridOn;
	    }

        public Sprite GetSpriteOfTilemap()
        {
            return new Sprite(Game, TileTextureFilename, new Point(Tilemap.TileWidth, Tilemap.TileHeight), new Point(Tilemap.MaxID, 1));
        }

        public override void Draw(GameTime gameTime, Camera camera)
        {
            Game.GraphicsDevice.Clear(Color.Blue);
            DrawTileMap(camera);
            base.Draw(gameTime, camera);
            if (_isGridOn) DrawGrid(camera);
        }
    }
}
