﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PGOCS.Collisions;
using PGOCS.Components;
using PGOCS.Graphics;

namespace PGOCS.Scenes
{
    public abstract class Scene
    {
        protected List<GameObject> GOList;
	    private readonly List<GameObject> _GOsToRemove;
        private readonly List<GameObject> _GOsToAdd;
	    private List<Coroutine> _coroutines;
	    private List<Coroutine> _coroutinesToRemove;
        protected RenderTarget2D RenderTarget;
        protected SpriteBatch SpriteBatch;
        protected Camera MainCamera;
	    protected CollisionSystem CollisionSystem;
        protected Vector2 Size { get; set; }
        public float Scale { get; private set; }
	    protected Overlay Overlay;
        public IGame Game;

        // TODO: make sure there are no problems with this overflowing
        protected int SceneTimer { get; private set; }

	    protected Scene(IGame game, float scale = 1f)
        {
            Game = game;
			CollisionSystem = new CollisionSystem();
            SpriteBatch = new SpriteBatch(game.GraphicsDevice);
            MainCamera = new Camera("mainCamera", game, this,
                new Vector2(game.GraphicsDevice.Viewport.Width/scale, 
                    game.GraphicsDevice.Viewport.Height/scale));
            GOList = new List<GameObject>();
			_GOsToRemove = new List<GameObject>();
            _GOsToAdd = new List<GameObject>();
			_coroutines = new List<Coroutine>();
			_coroutinesToRemove = new List<Coroutine>();
			Overlay = new Overlay(game, this);
            Scale = scale;
        }

        public virtual void Initialize() 
        {
            LoadContent();
        }

        public virtual void Draw(GameTime gameTime, Camera camera)
        {
	        if (GOList == null) return;
	        SpriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(1f));
	        foreach (GameObject g in GOList.Where(g => g != null))
		        g.Draw(SpriteBatch, camera);
			Overlay.Draw(SpriteBatch, gameTime);
	        SpriteBatch.End();
        }

        public virtual void Draw(GameTime gameTime)
        {
            Draw(gameTime, MainCamera); 
        }

        public virtual void Update(GameTime gameTime)
        {
			DebugConsole.Update();
            SceneTimer += gameTime.ElapsedGameTime.Milliseconds;
	        foreach (Coroutine coroutine in _coroutines)
	        {
		        coroutine.Update(gameTime);
		        if (coroutine.Ended) _coroutinesToRemove.Add(coroutine);
	        }
			RemoveStaleCoroutines();
            // Determine what is on a moving platform
            CollisionSystem.SetOnMovingPlatformStatus(GOList);
            foreach (GameObject go in GOList)
                go.Update(gameTime);
            foreach (GameObject go in _GOsToAdd)
                Add(go);
            _GOsToAdd.Clear();
            CollisionSystem.PreUpdate();
            CollisionSystem.UpdatePhysicalBodies(gameTime, GOList);
			DeleteObjects();
		}

	    public virtual void LateUpdate(GameTime gameTime)
	    {
			foreach (GameObject go in GOList)
				go.LateUpdate(gameTime);
			MainCamera.Update(gameTime, Size);
	    }

        protected virtual void LoadContent()
        {
            if (Size == Vector2.Zero)
                Size = new Vector2(Game.GraphicsDevice.Viewport.Width/Scale, Game.GraphicsDevice.Viewport.Height/Scale);
        }

        protected virtual void UnloadContent()
        {

        }

        public virtual void ResetScene()
        {
            SceneTimer = 0;
	        GOList.RemoveAll(x => true);
			Initialize();
        }

        public void Add(GameObject go)
        {
            GOList.Add(go);
        }

        public void AddLater(GameObject go)
        {
            _GOsToAdd.Add(go);
        }

        public void Remove(GameObject go)
        {
            _GOsToRemove.Add(go);
        }

	    private void RemoveStaleCoroutines()
	    {
		    foreach (Coroutine coroutine in _coroutinesToRemove)
		    {
			    _coroutines.Remove(coroutine);
		    }
			_coroutinesToRemove.Clear();
	    }

	    private void DeleteObjects()
	    {
		    foreach (GameObject go in _GOsToRemove)
			    GOList.Remove(go);
			_GOsToRemove.Clear();
	    }

        public Camera GetMainCamera()
        {
            return MainCamera;
        }

        public GameObject FindGameObject(string Id)
        {
            return GOList.Find(go => go.Name.Equals(Id));
        }

        public IEnumerable<T> FindGameObjectsByType<T>() where T : GameObject
        {
            return GOList.OfType<T>();
        }
    }
}