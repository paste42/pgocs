﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PGOCS.Components;

namespace PGOCS.Graphics
{
	public class Overlay
	{
		private readonly List<GameObject> _overlayObjects;
		private readonly Camera _overlayCamera;

		public Overlay(IGame game, Scenes.Scene scene)
		{
			_overlayObjects = new List<GameObject>();
			_overlayCamera = new Camera("overlayCamera", game, scene);
		}

		public void Add(GameObject go)
		{
			if (!_overlayObjects.Contains(go)) _overlayObjects.Add(go);
		}

		public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			if (_overlayObjects != null)
			foreach (GameObject go in _overlayObjects)
				go.GetComponent<Sprite>().Draw(spriteBatch, go.Transform, _overlayCamera);
		}
	}
}
