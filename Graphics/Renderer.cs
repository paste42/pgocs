﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PGOCS.Graphics
{
    public class Renderer
    {
        SpriteBatch spriteBatch;
        RenderTarget2D renderTarget;

        public Renderer(IGame game, float scale)
        {
            spriteBatch = new SpriteBatch(game.GraphicsDevice);
			renderTarget = new RenderTarget2D(game.GraphicsDevice, game.GraphicsDevice.Viewport.Width/(int)scale,
				game.GraphicsDevice.Viewport.Height/(int)scale, true, game.GraphicsDevice.DisplayMode.Format, DepthFormat.Depth24);

        }

        public void Draw(Game game)
        {
            game.GraphicsDevice.SetRenderTarget(renderTarget);
            
        }

        public void PostDraw(IGame game, float scale)
        {
            game.GraphicsDevice.SetRenderTarget(null);
			game.GraphicsDevice.Clear(Color.Green);
	        spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null);
			spriteBatch.Draw(renderTarget, new Rectangle(0,0, game.GraphicsDevice.Viewport.Width,
				game.GraphicsDevice.Viewport.Height), Color.White);
            spriteBatch.End();

        }
    }
}
