﻿using System.IO;
using Newtonsoft.Json;

namespace PGOCS.Config
{
    public class DirConfigLoader
    {
        private DirConfig configData;
        public DirConfig ConfigData { get { return configData; } }

        public DirConfigLoader(string filePath = "./Config/dirconfig.json")
        {
            configData = JsonConvert.DeserializeObject<DirConfig>(File.ReadAllText(filePath));
        }
    }
}
