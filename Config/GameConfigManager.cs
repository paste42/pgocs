﻿using System;

namespace PGOCS.Config
{
    public class GameConfigManager<TConfig>
    {
	    // ReSharper disable once StaticMemberInGenericType
        private static readonly object MyLock = new object();
        protected static TConfig Config;

        protected static T UseConfig<T>(Func<TConfig, T> f)
        {
            T result;
            lock(MyLock)
            {
                result = f(Config);
            }
            return result;
        }

        public static void SetConfig(TConfig config)
        {
            lock (MyLock)
            {
                Config = config;
            }
        }
    }
}
