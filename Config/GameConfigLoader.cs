﻿using System.IO;
using System.Threading;
using Newtonsoft.Json;

namespace PGOCS.Config
{
    public class GameConfigLoader<TConfig>
    {
        Thread thread;
        bool _exiting;
        FileInfo currentFileInfo;
        string filePath;
        private static GameConfigLoader<TConfig> _instance;
        int sleepTimeMilliseconds;
        
        protected GameConfigLoader(string filePath = @"./Config/gameconfig.json", int sleepTimeMilliseconds = 1000)
        {
            thread = new Thread(CheckFileChanges);
            _exiting = false;
            this.filePath = filePath;
            currentFileInfo = new FileInfo(filePath);
            this.sleepTimeMilliseconds = sleepTimeMilliseconds;
        }

        public static GameConfigLoader<TConfig> Instance
        {
            get
            {
                if(_instance == null) _instance = new GameConfigLoader<TConfig>();
                return _instance;
            }
        }

        public void Start()
        {
            thread.Start();
        }

        public void Stop()
        {
            _exiting = true;
        }

        protected void CheckFileChanges()
        {
            FileInfo newFileInfo;
            while(!_exiting)
            {
                newFileInfo = new FileInfo(filePath);
                if (currentFileInfo.LastWriteTime < newFileInfo.LastWriteTime)
                {
                    LoadConfig();
                }
                Thread.Sleep(sleepTimeMilliseconds);
            }
        }

        public void LoadConfig()
        {
            TConfig tConfig = JsonConvert.DeserializeObject<TConfig>(File.ReadAllText(filePath));
            GameConfigManager<TConfig>.SetConfig(tConfig);
        }
    }
}
