﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using PGOCS.Config;
using PGOCS.Scenes;

namespace PGOCS
{
    public interface IGame
    {
        GraphicsDevice GraphicsDevice { get; }
        ContentManager Content { get; set; }
        DirConfigLoader DirConfig { get; }
        void SwitchScene(Scene scene);
    }
}
