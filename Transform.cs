﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace PGOCS
{
    public class Transform : GOComponent
    {
        /// <summary>
        /// List of child transforms
        /// </summary>
        List<Transform> children;

        public Transform(Vector2 position = new Vector2(), float depth = 0f, float rotation = 0f)
            : base("Transform")
        {
            this.Position = position;
            this.Depth = depth;
            this.Rotation = rotation;
            children = new List<Transform>();
        }

        public override void Update(GameTime gameTime)
        {
            
        }
        
        /// <summary>
        /// Combines two transforms' positions and rotations
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public static Transform Compose(Transform parent, Transform child)
        {
            Transform result = new Transform(parent.Position + child.Position, parent.Depth + child.Depth, parent.Rotation + child.Rotation);
            result.Rotation -= (float)Math.Floor(result.Rotation);
            if ((result.Depth < 0f) || (result.Depth > 1.0f))
            {
                Debug.WriteLine("Depth out of bounds");
                result.Depth = MathHelper.Clamp(result.Depth, 0f, 1.0f);
            }
            return result; 
        }
        
        public static Transform operator -(Transform t)
        {
            return new Transform(-t.Position, t.Depth, 1 - t.Rotation);
        }

        /// <summary>
        /// position in world space
        /// </summary>
        public Vector2 Position { get; set; }
        
        /// <summary>
        /// rotation around the z axis, clockwise in radians
        /// </summary>
        public float Rotation { get; set; }

        /// <summary>
        /// depth from 0 to 100000, 0 being the absolute front and 100000 being the absolute back
        /// </summary>
        public float Depth { get; set; }

        private Transform myParent;
        /// <summary>
        /// The parent transform of this transform
        /// </summary>
        public Transform Parent
        {
            get { return myParent; }
            set
            {
                if (value == null)
                {
                    myParent.children.Remove(this);
                    myParent = null;
                }
                else
                {
                    myParent = value;
                    myParent.children.Add(this);
                }
            }
        }

        /// <summary>
        /// Returns the child Transform at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Transform GetChild(int index)
        {
            return children[index];
        }

        public Transform GetAbsoluteTransform()
        {
            if (Parent != null)
                return Transform.Compose(Parent, this);
            return this;
        }
    }
}
